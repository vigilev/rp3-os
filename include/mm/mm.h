#ifndef MM_H
#define MM_H
#include <periph/base.h>

#define LOW_MEMORY  (0x80000)
#define HIGH_MEMORY (PBASE)
#define PAGE_SIZE   (0x1000)
#define STACK_SIZE  PAGE_SIZE

#ifndef __ASSEMBLER__

#ifdef __TEST__
#define PAGES ((HIGH_MEMORY - LOW_MEMORY) / PAGE_SIZE)
extern volatile unsigned char page_bitmap[PAGES];
#endif

void pages_init();
void* page_alloc(void);
void page_free(void*);
#endif

#endif