#ifndef LIB_IO_H
#define LIB_IO_H

typedef struct io {
    void (*putc)(char);
    char (*getc)();
} io_t;


#endif