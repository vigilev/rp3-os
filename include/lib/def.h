#ifndef LIB_DEF_H
#define LIB_DEF_H

#define NULL  ((void*)0x0)

#define bool  int
#define true  1
#define false 0


// errors
#define err_t int
#define EOK     0
#define ENULL   1 // null
#define ENREADY 2 // thread not ready
#define EJOINED 3 // thread is already joined
#define ENFINIS 4 // thread status is not finished
#define ETSWITC 5 // thread didn't switch to another
#define ECORE   6 // bad number of core
#define ESCHCUR 7 // scheduled current thread
#define ECURNSC 8 // current thread is not scheduled

#endif