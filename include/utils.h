#ifndef UTILS_H
#define UTILS_H

#ifndef __ASSEMBLER__
unsigned int get_el();
unsigned int get_core_id();
unsigned int get_sp();
#endif

#endif