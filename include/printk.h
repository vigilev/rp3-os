#ifndef LIB_PRINTK_H
#define LIB_PRINTK_H
#include <lib/def.h>
#include <lib/io.h>


unsigned int ioprintk(io_t s, char* f, ...);
void init_printk(io_t s);
unsigned int printk(char* f, ...);

#endif