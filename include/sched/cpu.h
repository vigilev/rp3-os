#ifndef SCHED_CPU_H
#define SCHED_CPU_H

struct cpu_context {
    unsigned long gp_regs[31]; // general purpose registers
    unsigned long sp; // stack pointer
};
void cpu_switch_context(struct cpu_context* from, struct cpu_context* to);

#endif