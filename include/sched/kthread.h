#ifndef PROC_KTHREAD_H
#define PROC_KTHREAD_H

#include <lib/def.h>
#include <sched/cpu.h>

enum kthread_status {
    kthread_init,
    kthread_ready,
    kthread_running,
    kthread_waiting,
    kthread_finished,
    kthread_joined
};

typedef struct kthread {
    struct cpu_context context;
    enum kthread_status status;
    struct kthread *joining_with; // kthread which this thread is waiting to join with
    void* (*job)(void*);
    void* arg;
    void** return_address;
    unsigned int counter;
    unsigned int priority;
    unsigned int preferred_core;
} kthread_t;

extern kthread_t * volatile current_core_thread[];

#define NCORES  4
#define CURRENT_KTHREAD current_core_thread[get_core_id()]


void set_initial_kthread();
err_t kthread_create(kthread_t *alloc_page, void* (*fn)(void*), void* arg);
void kthread_yield();
err_t kthread_join(kthread_t* thread, void** result_ptr);
err_t kthread_switch_to(kthread_t* thread);
void kthread_exit();
#endif