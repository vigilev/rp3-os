#ifndef SCHED_SCHED_H
#define SCHED_SCHED_H
#include <sched/kthread.h>


#ifdef __TEST__
#define NR_THREADS 10
extern kthread_t * volatile kthreads[NCORES][NR_THREADS];
#endif


err_t schedule_next();
err_t schedule(kthread_t *th);
void  unschedule(kthread_t *th);

#endif