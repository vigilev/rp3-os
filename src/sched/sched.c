#include <sched/sched.h>
#include <sched/kthread.h>
#include <utils.h>
#include <printk.h>
/**
Non preemptive round-robin scheduler.
TODO: add locks for intercore scheduling
TODO: add suppoting of interrupts
**/

#define NR_THREADS 10
#ifndef __TEST__
static
#endif
kthread_t * volatile kthreads[NCORES][NR_THREADS];


err_t schedule_next(){
    int curr_th_idx = -1;

    // find running thread
    for (int i = 0; i < NR_THREADS; i++){
        if(kthreads[get_core_id()][i] == CURRENT_KTHREAD){
            curr_th_idx = i;
            break;
        }
    }
    if (curr_th_idx < 0){
        return ECURNSC;
    }

    int next_th_idx = -1;
    for(int i = (curr_th_idx + 1) % NR_THREADS; i < NR_THREADS; i = (i + 1) % NR_THREADS){
        if (i == curr_th_idx){
            break;
        }

        // unlock waiting thread
        if(kthreads[get_core_id()][i] && kthreads[get_core_id()][i]->status == kthread_waiting
           && kthreads[get_core_id()][i]->joining_with 
           && kthreads[get_core_id()][i]->joining_with->status == kthread_finished) {
            kthreads[get_core_id()][i]->status = kthread_ready;
        }

        if(kthreads[get_core_id()][i] && kthreads[get_core_id()][i]->status == kthread_ready){
            next_th_idx = i;
            break;
        }
    }
    if(next_th_idx < 0){
        return ESCHCUR;
    }
    return kthread_switch_to(kthreads[get_core_id()][next_th_idx]);
}


err_t schedule(kthread_t *th) {
    if(th == NULL){
        return ENULL;
    }
    if(th->preferred_core >= NCORES || th->preferred_core < 0){
        return ECORE;
    }

    bool scheduled = false;
    for(int i = 0; i < NR_THREADS; i++){
        if(th == kthreads[th->preferred_core][i]){
            scheduled = true;
            break;
        }
    }
    if(!scheduled){
        for(int i = 0; i < NR_THREADS; i++){
            if(!kthreads[th->preferred_core][i]){
                kthreads[th->preferred_core][i] = th;
                break;
            }
        }
    }
    return EOK;
}


void  unschedule(kthread_t *th) {
    if(th == NULL || th->preferred_core >= NCORES || th->preferred_core < 0){
        return;
    }

    for(int i = 0; i < NR_THREADS; i++){
        if(th == kthreads[th->preferred_core][i]){
            kthreads[th->preferred_core][i] = NULL;
        }
    }
}