#include <sched/kthread.h>
#include <sched/sched.h>
#include <utils.h>
#include <mm/mm.h>

#include <printk.h>

kthread_t * volatile current_core_thread[NCORES];

static void kthread_stub(){
    do{
        if(!CURRENT_KTHREAD->job)
            break;
        void* result = CURRENT_KTHREAD->job(CURRENT_KTHREAD->arg);
        
        while(!CURRENT_KTHREAD->return_address) {
            kthread_yield();
        }
        *(CURRENT_KTHREAD->return_address) = result;
    } while(0);
    while(1) kthread_exit();
}

void set_initial_kthread() {
    CURRENT_KTHREAD = (kthread_t *)(((unsigned long)get_sp() / PAGE_SIZE) * PAGE_SIZE);
    CURRENT_KTHREAD->status = kthread_running;
    CURRENT_KTHREAD->joining_with = NULL;
    CURRENT_KTHREAD->job = NULL;
    CURRENT_KTHREAD->arg = NULL;
    CURRENT_KTHREAD->return_address = NULL;
    CURRENT_KTHREAD->preferred_core = get_core_id();
}


err_t kthread_create(kthread_t *alloc_page, void* (*fn)(void*), void* arg){
    if(!alloc_page || !fn){
        return ENULL;
    }

    for (int i = 0; i < 31; i++){
        alloc_page->context.gp_regs[i] = 0;
    }

    alloc_page->context.sp = ((unsigned long) alloc_page) + PAGE_SIZE;
    alloc_page->context.gp_regs[29] = alloc_page->context.sp; // fp
    alloc_page->context.gp_regs[30] = (unsigned long) kthread_stub; // lr

    alloc_page->job = fn;
    alloc_page->arg = arg;
    alloc_page->joining_with = NULL;
    alloc_page->return_address = NULL;

    alloc_page->preferred_core = CURRENT_KTHREAD->preferred_core;

    alloc_page->status = kthread_init;
    return EOK;
}


err_t kthread_join(kthread_t* thread, void** result_ptr) {
    err_t err = EOK;
    do {
        if(thread == CURRENT_KTHREAD){
            return EJOINED;
        }
        if(!thread || !result_ptr) {
            err = ENULL;
            break;
        }
        if(thread->status != kthread_ready){
            err = ENREADY;
            break;
        }
        if(thread->return_address || thread->status == kthread_joined){
            err = EJOINED;
            break;
        }
        
        CURRENT_KTHREAD->status = kthread_waiting;
        CURRENT_KTHREAD->joining_with = thread;
        thread->return_address = result_ptr;
        if ((err = schedule(thread)) != EOK) {
            break;
        }
        while(thread->status != kthread_finished){
            kthread_yield();
        }
        if(thread->status != kthread_finished){
            err = ENFINIS;
            break;
        }
        if(thread->status == kthread_joined){
            err = EJOINED;
            break;
        }
        thread->status = kthread_joined;
        CURRENT_KTHREAD->joining_with = NULL;
        unschedule(thread);
    } while(0);
    CURRENT_KTHREAD->status = kthread_running;
    return err;
}


err_t kthread_switch_to(kthread_t* thread) {
    err_t err = EOK;
    do {
        if(!thread) {
            err = ENULL;
            break;
        }

        if(CURRENT_KTHREAD->status == kthread_running){
            CURRENT_KTHREAD->status = kthread_ready;
        }
        if(thread->status == kthread_ready){
            thread->status = kthread_running;
        }
        kthread_t *old_kthread = CURRENT_KTHREAD;
        CURRENT_KTHREAD = thread;
        
        cpu_switch_context(&(old_kthread->context), &(CURRENT_KTHREAD->context));
    } while(0);
    return err;
}


void kthread_exit() {
    CURRENT_KTHREAD->status = kthread_finished;
    kthread_yield();
}


void kthread_yield() {
    schedule_next();
}