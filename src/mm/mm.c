#include <lib/def.h>
#include <mm/mm.h>

extern char _end[];

#define PAGES ((HIGH_MEMORY - LOW_MEMORY) / PAGE_SIZE)

#ifndef __TEST__
static
#endif
volatile unsigned char page_bitmap[PAGES];

void pages_init() {
    page_bitmap[0] = page_bitmap[1] = page_bitmap[2] = page_bitmap[3] = 1;
}


void* page_alloc(){
    for (int i = 0; i < PAGES; i++){
        if(!page_bitmap[i]){
            page_bitmap[i] = 1;
            return (void*)_end + i * PAGE_SIZE;
        }
    }
    return NULL;
}


void page_free(void* ptr){
    if(!ptr)
        return;
    unsigned long ptr_long = (unsigned long)ptr;
    unsigned int page_number = (ptr_long - LOW_MEMORY) / PAGE_SIZE;
    if (page_number >= PAGES) return;
    page_bitmap[page_number] = 0;
}
