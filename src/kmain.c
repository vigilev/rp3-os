#include <utils.h>
#include <periph/uart.h>
#include <sched/kthread.h>
#include <lib/io.h>
#include <printk.h>


typedef enum core_state {
    core_not_started = 0x0,
    core_finished
} core_state_t;

volatile unsigned long jump_address;
volatile core_state_t cores[NCORES] = {0,};

#ifdef __TEST__
#include <tests.h>
void kmain() {
    unsigned int core_id = get_core_id();
    if(core_id == 0) {
        uart_init();
        io_t uart_io = {
            .putc = uart_send_byte,
            .getc = uart_receive_byte
        };
        init_printk(uart_io);
    } else {
        asm volatile ("wfe");
    }
    run_tests();
}

#else

void kmain(){
    unsigned int core_id = get_core_id();
    if(core_id == 0) {
        uart_init();
        io_t uart_io = {
            .putc = uart_send_byte,
            .getc = uart_receive_byte
        };
        init_printk(uart_io);
    } else {
        while(cores[core_id - 1] != core_finished){
            asm volatile ("wfe");
        }
    }
    printk("core: %x, EL: %d, sp: %p\n", get_core_id(), get_el(), get_sp());
    cores[core_id] = core_finished;
    asm volatile ("sev");
    while(1) asm volatile ("wfe");
}
#endif
