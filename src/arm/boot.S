#include "mm/mm.h"
#include "arm/sysregs.h"
.section ".text.boot"
.globl Start

Start:
    bl   save_boot_regs

    adr  x0, init_sp
    bl   init_el1

init_sp:
    mrs  x19, mpidr_el1
    and  x19, x19, #0xFF

    mov  x18, #LOW_MEMORY
    add  x18, x18, #STACK_SIZE
    mov  x17, #STACK_SIZE // x17 <- stacksize
    mul  x17, x17, x19 // x17 <- x17 * core_id
    add  x18, x18, x17 // x18 <- x18 + x17
    mov  sp, x18

clean_bss:
    adr x0, _bss_start
    adr x1, _bss_end
    sub x1, x1, x0
1:
    str xzr, [x0]
    add x0, x0, #8
    sub x1, x1, #8
    cbnz x1, 1b


kernel_init:
    bl load_boot_regs
    bl kmain

2:  b   2b
