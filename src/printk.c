#include <lib/def.h>
#include <lib/io.h>
#include <printk.h>
#include <stdarg.h>
#include <sched/kthread.h>
#include <stddef.h>

err_t ulong_to_string(
        char buf[],
        unsigned int buf_size,
        unsigned long value,
        unsigned int base,
        unsigned width,
        bool capitalize) {
    
    // main algorithm
    if(value == 0 && width == 0) {
        buf[0] = '0';
        buf[1] = '\0';
        return EOK;
    }

    unsigned int len = 0;
    for(;value && len < buf_size - 1; len++){
        buf[len] = ((value % base) < 10 ?
                    '0' : ((capitalize ? 'A' : 'a') - 10))
                        + (value % base);
        value /= base;
    }

    // leading zeros
    if (width > 0 && !value) {
        for(;len < width && len < buf_size - 1; len++){
            buf[len] = '0';
        }
    }

    //reverse string
    for (unsigned int i = 0; i < len/2; i++){
        char t = buf[i];
        buf[i] = buf[len - 1 - i];
        buf[len - 1 - i] = t;
    }
    buf[len] = '\0';
    return EOK;
}

unsigned int io_puts(io_t stream, char* str){
    unsigned int result = 0;
    for(char *cur = str; *cur; cur++){
        stream.putc(*cur);
        result++;
    }
    return result;
}

unsigned int general_printk(io_t stream, char* format, va_list args){
    unsigned int result = 0;
    for(char *cur = format; *cur; cur++){
        if(*cur == '%'){
            char* next = cur + 1;
            // unsigned int in hex
            if(*next == 'x' || *next == 'X') {
                unsigned int value = va_arg(args, unsigned int);
                char buf[2 * sizeof(unsigned int) + 1];
                ulong_to_string(buf, 2 * sizeof(unsigned int) + 1, value, 16, 0, *next == 'X');
                result += io_puts(stream, buf);
            } 
            // pointer i.e. 0x(unsigned long) in hex
            else if (*next == 'p' || *next == 'P') {
                result += io_puts(stream, "0x");
                unsigned long value = va_arg(args, unsigned long);
                char buf[2 * sizeof(unsigned long) + 1];
                ulong_to_string(buf, 2 * sizeof(unsigned long) + 1, value, 16, 2 * sizeof(unsigned long), *next == 'P');
                result += io_puts(stream, buf);
            }
            // int in decimal
            else if (*next == 'd') {
                int value = va_arg(args, int);
                if(value < 0){
                    result += io_puts(stream, "-");
                    value *= -1;
                }
                char buf[11];
                ulong_to_string(buf, 11, value, 10, 0, false);
                result += io_puts(stream, buf);
            }
            // single char
            else if (*next == 'c') {
                char value = (char)va_arg(args, int);
                stream.putc(value);
                result++;
            }
            // char[]
            else if (*next == 's') {
                char *str = va_arg(args, char*);
                result += io_puts(stream, str);
            }
            else if (*next == 'T') {
                kthread_t *th = va_arg(args, kthread_t*);
                result += ioprintk(stream, "[kthread: %p]\n\r", th);
                for(unsigned int i = 0; i < 31; i++){
                    result += ioprintk(stream, "[+%d x%d: %p]\n\r", offsetof(kthread_t, context) + offsetof(struct cpu_context, gp_regs) + i*sizeof(unsigned long), i, th->context.gp_regs[i]);
                }
                result += ioprintk(stream, "[+%d sp: %p]\n\r", offsetof(kthread_t, context) + offsetof(struct cpu_context, sp), th->context.sp);
                result += ioprintk(stream, "[%p]", th + sizeof(kthread_t));
            }
            // %
            else if (*next == '%') {
                stream.putc('%');
                result++;
            }
            cur += 1;
        } else {
            stream.putc(*cur);
            result++;
        }
    }
    return result;
}

unsigned int ioprintk(io_t stream, char *format, ...) {
    va_list args;
    va_start(args, format);
    return general_printk(stream, format, args);
}



io_t printk_stream;

void init_printk(io_t stream){
    printk_stream = stream;
}

unsigned int printk(char *format, ...) {
    va_list args;
    va_start(args, format);
    return general_printk(printk_stream, format, args);
}