BUILD_DIR = build
SRC_DIR = src
INCLUDE_DIR = include

ARMGNU ?= aarch64-linux-gnu
COPS = -Wall -nostdlib -nostartfiles -ffreestanding -I$(abspath $(INCLUDE_DIR)) -mgeneral-regs-only -O2 -g
ASMOPS = -Iinclude
export ARMGNU
export COPS
export ASMOPS

C_FILES = $(wildcard $(shell find $(SRC_DIR) -name "*.c"))
ASM_FILES = $(wildcard $(shell find $(SRC_DIR) -name "*.S"))
OBJ_FILES = $(C_FILES:$(SRC_DIR)/%.c=$(BUILD_DIR)/%_c.o)
OBJ_FILES += $(ASM_FILES:$(SRC_DIR)/%.S=$(BUILD_DIR)/%_s.o)
DEP_FILES = $(OBJ_FILES:%.o=%.d)
-include $(DEP_FILES)

# TESTS related variables
TESTS_DIR = tests

ifdef TESTS_OBJ_FILES
ifdef TEST_MACRO
COPS += -D$(TEST_MACRO) -I$(TESTS_DIR)
ASMOPS += -D$(TEST_MACRO) -I$(TESTS_DIR)
endif
OBJ_FILES += $(addprefix $(TESTS_DIR)/,$(TESTS_OBJ_FILES))
endif


# RULES
.PHONY: all clean minicom deploy test

all: kernel8.img


$(BUILD_DIR)/%_c.o: $(SRC_DIR)/%.c
	mkdir -p $(@D)
	$(ARMGNU)-gcc $(COPS) -MMD -c $< -o $@


$(BUILD_DIR)/%_s.o: $(SRC_DIR)/%.S
	mkdir -p $(@D)
	$(ARMGNU)-gcc $(ASMOPS) -MMD -c $< -o $@


kernel8.img: $(SRC_DIR)/linker.ld $(OBJ_FILES)
	$(ARMGNU)-ld -T $(SRC_DIR)/linker.ld -o $(BUILD_DIR)/kernel8.elf  $(OBJ_FILES)
	$(ARMGNU)-objcopy $(BUILD_DIR)/kernel8.elf -O binary kernel8.img
	cp kernel8.img ./firmware/kernel8.img


list:
	@echo C_FILES $(C_FILES)
	@echo ASM_FILES $(ASM_FILES)
	@echo OBJ_FILES $(OBJ_FILES)


deploy: kernel8.img scripts/Makefile
	make -C ./scripts deploy


test:
	make -C $(TESTS_DIR)


clean:
	rm -rf $(BUILD_DIR) *.img
	make -C $(TESTS_DIR) clean
