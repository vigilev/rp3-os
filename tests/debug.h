#ifndef DEBUG_H
#define DEBUG_H
#include <printk.h>

#define panic(text) printk("%s %s %d: %s", __FILE__, __func__, __LINE__, text); while(1) asm volatile ("nop");

#define assert_string(expected, actual, expect_length) for(int i = 0; i < expect_length; i++) { \
                if (actual[i] == '\0' || expected[i] != actual[i]) { \
                    printk("%s %s %d:\n\r\texpect:%s\n\r\tactual:%s\n\r", __FILE__, __func__, __LINE__, expected, actual);\
                    while(1) asm volatile ("nop");\
                    }\
                } 

#define assert_number(actual, expected) if(expected != actual) {\
                printk("%s %s %d:\n\r\texpect:0x%x\n\r\tactual:0x%x\n\r", __FILE__, __func__, __LINE__, expected, actual);\
                while(1) asm volatile ("nop");\
                }

#define assert(condition) if(!(condition)) {\
    printk("%s %s %d: assertion %s failed\n\r",__FILE__, __func__, __LINE__, #condition);\
    while(1) asm volatile ("nop");\
}

#define asserted_ok() printk("%s: ok\n\r", __func__);
#define asserted_fail() panic("assertion_fail");

#endif