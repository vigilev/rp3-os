#include <tests.h>
#include <printk.h>

void (*available_tests[])() = {
    test_printk
   ,test_initial_kthread
   ,test_kthread_switch_to
   ,test_schedule
   ,test_schedule_finished
   ,test_schedule_waiting
   ,test_thread_join
};

#define NTESTS 7

void run_tests() {
    printk("run %d tests:\n\r", NTESTS);
    for(int i = 0; i < NTESTS; i++){
        printk("%d ", i + 1);
        available_tests[i]();
    }
}