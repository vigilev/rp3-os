#ifndef TESTS_H
#define TESTS_H
#include <debug.h>


// run all tests
void run_tests();

// defined implemented tests
void test_printk();

// singlecore kthread api related tests
void test_initial_kthread();
void test_kthread_switch_to();
void test_schedule();
void test_schedule_finished();
void test_schedule_waiting();
void test_thread_join();
#endif