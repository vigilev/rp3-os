#ifdef __TEST__
#include <debug.h>
#include <printk.h>
#include <lib/io.h>
#include <periph/uart.h>

#define BUF_SIZE 300
char buf[BUF_SIZE];
unsigned int length = 0;

void buf_putc(char ch) {
    if (length >= BUF_SIZE){
        length = 0;
    }
    buf[length++] = ch;
}


/**
    Test correctness of formating in printk
*/
void test_printk() {
    io_t buf_io = {
        .putc = buf_putc,
        .getc = NULL
    };
    init_printk(buf_io);

    char expected[] = "0, -10, 13, 1234abcd, 1234ABCD, string, c, PTR: 0x0123456789ABCDEF, ptr: 0x0123456789abcdef";
    void* ptr = (void*)0x0123456789ABCDEF;
    printk("%d, %d, %d, %x, %X, %s, %c, PTR: %P, ptr: %p", 0, -10, 13, 0x1234abcd, 0x1234abcd, "string", 'c', ptr, ptr);

    // asserting
    uart_init();
    io_t uart_io = {
        .putc = uart_send_byte,
        .getc = uart_receive_byte
    };
    init_printk(uart_io);
    assert_string(expected, buf, 91);
    asserted_ok();
}

#endif