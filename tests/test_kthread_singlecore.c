#include <debug.h>
#include <tests.h>
#include <utils.h>
#include <mm/mm.h>
#include <printk.h>

#include <sched/kthread.h>
#include <sched/sched.h>

static void setup() {
    pages_init();
    set_initial_kthread();
    schedule(CURRENT_KTHREAD);
}

static void teardown() {
    
}

// check if the initial thread is created
void test_initial_kthread(){
    setup();

    assert(page_bitmap[get_core_id()] != 0);
    assert(get_sp() >= LOW_MEMORY + get_core_id()*PAGE_SIZE && get_sp() < LOW_MEMORY + (get_core_id() + 1) * PAGE_SIZE);
    assert(CURRENT_KTHREAD != NULL);
    assert(CURRENT_KTHREAD->status == kthread_running);
    assert(sizeof(kthread_t) < PAGE_SIZE);
    assert(kthreads[get_core_id()][0] == CURRENT_KTHREAD);
    asserted_ok();
    teardown();
}


volatile unsigned int value = 0;
kthread_t *old_kthread = NULL;
kthread_t *new_kthread = NULL;

void* job_for_kthread_switch_to(void* arg) {
    printk("%s started\n", __func__);
    value = 1;
    assert(old_kthread != NULL);
    assert(get_sp() >= (unsigned long)CURRENT_KTHREAD && get_sp() <= (unsigned long)CURRENT_KTHREAD + PAGE_SIZE);
    asserted_ok();
    kthread_switch_to(old_kthread);
    asserted_fail();
}


/// test if it is possible to switch between threads
void test_kthread_switch_to() {
    setup();
    new_kthread = (kthread_t*)page_alloc();
    assert(new_kthread != NULL);
    assert(!kthread_create(new_kthread, job_for_kthread_switch_to, NULL));
    
    new_kthread->status = kthread_ready;
    assert(new_kthread->job == job_for_kthread_switch_to);
    assert((unsigned long)(new_kthread->context.sp) == (unsigned long)new_kthread + PAGE_SIZE);
    
    // save current thread to go back to it
    old_kthread = CURRENT_KTHREAD;
    kthread_switch_to(new_kthread);
    assert(old_kthread == CURRENT_KTHREAD);
    assert(CURRENT_KTHREAD->status == kthread_running);

    // assert changes
    assert_number(value, 1);
    asserted_ok();

    // remove garbage
    page_free(new_kthread);
    new_kthread = NULL;
    old_kthread = NULL;
    teardown();
}


/// Scheduler test
int sched_shared_var = 0;
void* scheduler_job(void* arg){
    printk("%s started\n", __func__);
    sched_shared_var = 1;
    assert(kthreads[get_core_id()][0] == old_kthread);
    assert(kthreads[get_core_id()][1] == new_kthread);
    assert(CURRENT_KTHREAD == new_kthread);
    schedule_next();

    return NULL;
}

void test_schedule(){
    setup();

    assert(schedule_next() == ESCHCUR);

    old_kthread = CURRENT_KTHREAD;
    new_kthread = page_alloc();
    assert(new_kthread != NULL);
    assert(!kthread_create(new_kthread, scheduler_job, NULL));
    new_kthread->status = kthread_ready;
    
    assert(!schedule(new_kthread));
    assert(kthreads[get_core_id()][0] == old_kthread);
    assert(kthreads[get_core_id()][1] == new_kthread);
    assert(CURRENT_KTHREAD == old_kthread);

    assert(!schedule_next());
    unschedule(new_kthread);
    assert(kthreads[get_core_id()][1] == NULL);

    assert(schedule_next() == ESCHCUR);

    unschedule(CURRENT_KTHREAD);
    assert(schedule_next() == ECURNSC);
    schedule(CURRENT_KTHREAD);
    assert(kthreads[get_core_id()][0] == old_kthread);

    asserted_ok();

    page_free(new_kthread);
    old_kthread = NULL;
    new_kthread = NULL;
    teardown();
}

void test_schedule_finished(){
    setup();
    
    old_kthread = CURRENT_KTHREAD;
    new_kthread = page_alloc();
    assert(new_kthread != NULL);
    assert(!kthread_create(new_kthread, scheduler_job, NULL));
    new_kthread->status = kthread_finished;
    old_kthread->status = kthread_waiting;
    old_kthread->joining_with = new_kthread;

    assert(!schedule(new_kthread));
    assert(kthreads[get_core_id()][0] == old_kthread);
    assert(kthreads[get_core_id()][1] == new_kthread);
    
    assert(schedule_next() == ESCHCUR);

    asserted_ok();

    unschedule(new_kthread);
    page_free(new_kthread);
    CURRENT_KTHREAD->joining_with = NULL;
    old_kthread = NULL;
    new_kthread = NULL;
    sched_shared_var = 0;
    teardown();
}


void* schedule_waiting_job(void* arg){
    printk("%s started\n", __func__);
    sched_shared_var = 1;
    assert(kthreads[get_core_id()][0] == old_kthread);
    assert(kthreads[get_core_id()][1] == new_kthread);
    assert(CURRENT_KTHREAD == new_kthread);

    assert(new_kthread->status == kthread_running);
    assert(old_kthread->status == kthread_waiting);
    assert(old_kthread->joining_with == CURRENT_KTHREAD);
    assert(schedule_next() == ESCHCUR);

    CURRENT_KTHREAD->status = kthread_finished;
    schedule_next();

    asserted_fail();

    return NULL;
}

void test_schedule_waiting(){
    setup();
    
    old_kthread = CURRENT_KTHREAD;
    new_kthread = page_alloc();
    assert(new_kthread != NULL);
    assert(!kthread_create(new_kthread, schedule_waiting_job, NULL));
    new_kthread->status = kthread_ready;
    old_kthread->status = kthread_waiting;
    old_kthread->joining_with = new_kthread;

    assert(!schedule(new_kthread));
    assert(kthreads[get_core_id()][0] == old_kthread);
    assert(kthreads[get_core_id()][1] == new_kthread);
    
    assert(schedule_next() == EOK);
    assert(sched_shared_var == 1);
    asserted_ok();

    unschedule(new_kthread);
    page_free(new_kthread);
    CURRENT_KTHREAD->joining_with = NULL;
    old_kthread = NULL;
    new_kthread = NULL;
    teardown();
}


void* joint_thread_job(void* arg){
    printk("%s \n", __func__);
    assert(CURRENT_KTHREAD == new_kthread);
    assert(old_kthread->status == kthread_waiting);
    assert(old_kthread->joining_with == CURRENT_KTHREAD);
    
    kthread_yield();
    asserted_ok();
    return arg;
}

void test_thread_join() {
    setup();
    
    old_kthread = CURRENT_KTHREAD;
    new_kthread = page_alloc();
    assert(new_kthread != NULL);
    assert(!kthread_create(new_kthread, schedule_waiting_job, NULL));
    new_kthread->status = kthread_ready;
    old_kthread->status = kthread_waiting;
    old_kthread->joining_with = new_kthread;

    void* result = NULL;
    assert(kthread_join(CURRENT_KTHREAD, &result) == EJOINED);
    assert(kthread_join(new_kthread, &result) == EOK);
    assert(new_kthread->status == kthread_joined);
    assert(CURRENT_KTHREAD->joining_with == NULL);
    asserted_ok();

    unschedule(new_kthread);
    page_free(new_kthread);
    CURRENT_KTHREAD->joining_with = NULL;
    old_kthread = NULL;
    new_kthread = NULL;
    teardown();
}